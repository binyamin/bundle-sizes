# Bundle Sizes
> Comparing binary sizes for "hello-world" applications in various languages.

- **C++**:  20K
- **Deno (JS)**: 84M
- **Go**: 752K
- **Node.js**: 40M
- **Python**: 1.7M
- **Rust**: 3.5M

## Contributing
I'd like to see more languages tested. The code should just print "hi" to the console.