# Results

## Go
**Size**: 752K
```sh
GOOS=linux go build -ldflags="-s -w" -o main-1 ./main.go
```

## C++
**Size**: 20K
```sh
g++ main.cpp -o exe/cpp
```

## Rust
**Size**: 3.5M
```sh
cargo build --release
```

## Python
**Size**: 1.7M
```sh
pyinstaller hello.py
```

## JavaScript
### Deno
**Size**: 84M
```sh
deno compile test.js
```

### Node.js
**Size**: 40M
```sh
pkg test.js -o node
```